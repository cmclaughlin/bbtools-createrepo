This is bbtools.
================

Run "python setup.py install".

You'll need a ~/.bbtools config file of the form::

  [user]
  username = <your_bb_username>
  password = <your_bb_password>
