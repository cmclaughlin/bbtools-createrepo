import os

from configobj import ConfigObj

def get_config():
	fn = os.path.expanduser('~/.bbtools')

	return ConfigObj(fn)

def to_repo(s):
	if '/' in s:
		return s
	
	cfg = get_config()

	return cfg.get('user').get('username')+'/'+s