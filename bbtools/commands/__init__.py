import os
import json
import jinja2
import requests

BASE_URL = 'https://api.bitbucket.org/1.0'

PKG_DATA_DIRS = [os.path.join(os.path.dirname(__file__), 'templates')]

ENV = jinja2.Environment(loader=jinja2.ChoiceLoader(
    [jinja2.FileSystemLoader(s) for s in PKG_DATA_DIRS]))
ENV.loader.loaders.append(jinja2.PackageLoader("bbtools", "templates"))

class Command(object):
	def __init__(self, cfg):
		self.cfg = cfg
		self._auth = (cfg.get('user').get('username'), 
					  cfg.get('user').get('password'))

	def request(self, path='/', method='get', **kwargs):
		callee = getattr(requests, method)

		resp = callee(BASE_URL+path, auth=self._auth, **kwargs)

		if resp.ok and 'json' in resp.headers.get('content-type'):
			return json.loads(resp.content)
		elif resp.status_code < 300:
			return ''
		else:
			raise Exception(resp)

	def dispatch(self, subcmd, args):
		if subcmd:
			return getattr(self, subcmd)(args)
	
	def render(self, tmpl, ctx):
		t = ENV.get_template(tmpl)
		print t.render(**ctx)
