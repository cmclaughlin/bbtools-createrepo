from . import Command
from bbtools.deserializers import User

class Commands(Command):
	def profile(self, args):
		pl = self.request('/users/%s/' % (args.username,))
		user = User(pl.get('user'))

		try:
			import Image
			from fabulous import image
			from urllib2 import urlopen
			from tempfile import NamedTemporaryFile
			from os import unlink

			avatar = urlopen(user.avatar.replace("s=32","s=64")).read()
			tmp = NamedTemporaryFile(delete=False)
			tmp.write(avatar)
			tmp.close()

			i = image.Image(tmp.name)
			unlink(tmp.name)
			avatar = str(i)
		except ImportError:
			avatar = "\n(Install PIL for avatar support!)"
		finally:
			self.render('user/profile.tmpl', {'user': user, 'avatar': avatar})

def hookargs(command_sub_parsers):
	profile = command_sub_parsers.add_parser('profile', help='display a user\'s profile')
	profile.set_defaults(subcmd='profile')
	profile.add_argument('username')
