from . import Command

from bbtools.deserializers import User
from bbtools.utils import to_repo

class Commands(Command):
	def users(self, args):
		perms = self.request('/privileges/'+to_repo(args.repo))

		for perm in perms:
			user = User(perm.get('user'))

			print "%s (%s)" % (user.username, perm.get('privilege'))

	def grant(self, args):
		self.request('/privileges/'+to_repo(args.repo)+'/'+args.username, 
			method='put', data=args.privilege)

		self.users(args)

	def revoke(self, args):
		self.request('/privileges/'+to_repo(args.repo)+'/'+args.username,
			method='delete')

		self.users(args)

def hookargs(parser):
	users = parser.add_parser('users', help="List users of a repo")
	users.set_defaults(subcmd='users')
	users.add_argument('repo')

	grant = parser.add_parser('grant', help="Grant permissions on a repo")
	grant.set_defaults(subcmd='grant')
	grant.add_argument('repo')
	grant.add_argument('username')
	grant.add_argument('privilege')

	revoke = parser.add_parser('revoke', help='Revoke access to a user')
	revoke.set_defaults(subcmd='revoke')
	revoke.add_argument('repo')
	revoke.add_argument('username')