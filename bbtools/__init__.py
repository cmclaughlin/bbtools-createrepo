import os
import sys
import argparse
import importlib

from bbtools.utils import get_config

PR = 'bbtools.commands.%s'
cmds = dict()

def main():
	cfg = get_config()

	parser = argparse.ArgumentParser(description='BB command line tools.')
	sub_parsers = parser.add_subparsers()

	# TODO: make sure username and password exists
	cmd_dir = os.path.join(os.path.dirname(__file__), 'commands/')

	for fn in os.listdir(cmd_dir):
		if fn.endswith('.py') and not fn.startswith('_'):
			bn = fn.split('.py', 1)[0]

			try:
				mod = importlib.import_module(PR % bn)
				command_parser = sub_parsers.add_parser(bn, help="%s subcommand" % (bn,))
				command_parser.set_defaults(cmd=bn)
				command_sub_parsers = command_parser.add_subparsers()
				mod.hookargs(command_sub_parsers)

				cmds[bn] = mod
			except ImportError:
				print "Failed to load the %s module." % bn

	args = parser.parse_args()

	if args.cmd in cmds:
		inst = cmds[args.cmd].Commands(cfg)
		inst.dispatch(args.subcmd, args)

if __name__ == "__main__":
	main()
